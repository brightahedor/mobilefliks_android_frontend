package com.eagleproductions.mobilefliks;

import java.io.File;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;


public class DatabaseCls {

	private static final String Key_RowID="rowid";
	private static final String Key_VideoPath="videoPath";
	private static final String Key_VideoName="videoName";
	private static final String Database_Name="Vidoesdb";
	private static final int Database_Version=1;
	private static final String Database_Table="tbl_videodetail";
	
	private DbHelper ourhelper;
   	private final Context  ourContext;
   	private SQLiteDatabase ourDatabase;
   	
   	private static class DbHelper extends SQLiteOpenHelper{
   		
   		private DbHelper (Context ctx){
   			super(ctx,Database_Name,null,Database_Version);
   			
   		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL("CREATE TABLE "+Database_Table+" ("+
			         Key_RowID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+Key_VideoPath+
			         " TEXT NOT NULL"+Key_VideoName+
			         " TEXT NOT NULL);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXISTS "+Database_Table);
			onCreate(db);
		}
		
		
   	}
   	   public DatabaseCls(Context context){
   		   ourContext=context;
   	   }
   	   
   	   public DatabaseCls open(){
   		   ourhelper=new DbHelper(ourContext);
   		   ourDatabase=ourhelper.getWritableDatabase();
   		   return this;
   	   }
   	   public void close(){
   		   ourhelper.close();
   	   }
   	   public long createEntry(String path, String name){
   		   ContentValues cv=new ContentValues();
   		   cv.put(Key_VideoName, name);
   		   cv.put(Key_VideoPath, path);
   		   return ourDatabase.insert(Database_Table, null, cv);
   	   }
   	   
   	   public void getPathDetails(VideoFiles act){
   		   ArrayList<DataCredentials> data=new ArrayList<DataCredentials>();
   		   String name="",path="";
   		   String[] columns=new String[]{Key_VideoPath,Key_VideoName};
   		   Cursor c=ourDatabase.query(Database_Table, columns, null, null, null, null, null);
   		   for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
   			   
   			   name=c.getString(c.getColumnIndex(Key_VideoName));
   			   path=c.getString(c.getColumnIndex(Key_VideoPath));
   			File direct = new File(path);
   			   if(direct.exists()){
   			   data.add(new DataCredentials(name, path));
   			   }else{
   				   
   			   }
   		   }
   		   if(data.isEmpty()){
   			   act.displayResult("No Movies Downloaded");
   		   }else{
   			act.setData(data);   
   		   }
   		   
   	   }

	
}
