package com.eagleproductions.mobilefliks;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.eagleproductions.mobilefliks.R;

public class Splash extends Activity {

	SharedPreferences mprefs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		mprefs = getSharedPreferences("Credentials", MODE_PRIVATE);
		

		Thread timer = new Thread() {
			@Override
			public void run() {
				try {
					sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();

				} finally {
                     String result=mprefs.getString("AfterFirst", "Default");
                     if(result.equals("Default")){
                    	 startActivity(new Intent(Splash.this, WalkThrough.class));
                     }else{
                    	 startActivity(new Intent(Splash.this, Home.class));
                     }
					

				}

			}
		};
		timer.start();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}
