package com.eagleproductions.mobilefliks;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.protocol.ResponseDate;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;

import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.Adapter.ImageLoadAdapter;
import com.eagleproductions.mobilefliks.Adapter.Item;
import com.eagleproductions.mobilefliks.Adapter.ItemLoader;
import com.eagleproductions.mobilefliks.Adapter.MgsImageLoader;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

/*import com.mpowerpayments.mpower.*;*/

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class Home extends Activity {
	private ViewFlipper viewFlipper;
	ImageLoadAdapter mgrid;
	SharedPreferences mprefs;
	AdapterContextMenuInfo info;
	String pathFile, videaname;
	AlertDialog alert;
	int currentVersion;
	ArrayList<Item> dataItem;
	ArrayList<DataDetails> detaItem;
	LayoutInflater linf;
	GridView moviesGrid;
	GridView trendingMovies;
	ImageLoadAdapter trending;
	TextView tvLoading;
	Button Retry;
	// ArrayList<byte[]> img;
	byte[] img1;
	
	/*
	 * MPowerSetup setup = new MPowerSetup(); MPowerCheckoutStore store = new
	 * MPowerCheckoutStore();
	 */
	public static String url = "";
	Intent videos;
	private MgsImageLoader fetchImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//
		Parse.initialize(this, "X36WSd20k9qUS3K9QruKWVHIfP9lvUgjXv66sXl5",
				"ryVYLZkC6aVephck9OX5w0mV1gVOoZbPgZW0gTim");
		//

		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		mprefs = getSharedPreferences("Credentials", MODE_PRIVATE);
		String first=mprefs.getString("AfterFirst", "Default");
		String registration=mprefs.getString("email", "Default");
		if (first.equals("Default") || registration.equals("Default")){
		SharedPreferences.Editor mEditor=mprefs.edit();
		mEditor.putString("AfterFirst", "true");
		mEditor.commit();
		startActivity(new Intent(this,RegisterUser.class));
		}
		
		tvLoading=(TextView)findViewById(R.id.tvLoading);
		tvLoading.setTextColor(Color.WHITE);
		Retry=(Button)findViewById(R.id.btnRetry);
		Retry.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tvLoading.setVisibility(View.VISIBLE);
				Retry.setVisibility(View.INVISIBLE);
				loadParse();
			}
		});
		
		this.linf = LayoutInflater.from(this);
		videos = new Intent(Home.this, VideoFiles.class);
		this.fetchImage = new MgsImageLoader(this);
		dataItem = new ArrayList<Item>();

		
		Intent intent1 = new Intent(this, PayPalService.class);

		intent1.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_LIVE);
		intent1.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AY7N4BAIo6Qj4aiNJDSvwbEi2UvCuwBs4M0g_McCdjAkUWdBbTlxUb8vzcGc");

		startService(intent1);
		moviesGrid = (GridView) findViewById(R.id.gridViewMovies);
		 trendingMovies = (GridView) findViewById(R.id.gridViewTrending);
		//new ItemLoader(this).execute();
		 loadParse();
		// new ItemLoader().execute("");
		/*
		 * //mpower payment
		 * 
		 * setup.setMasterKey("dd6f2c90-f075-012f-5b69-00155d846600");
		 * setup.setPrivateKey("test_private_oDLVld1eNyh0IsetdhdJvcl0ygA");
		 * setup.setPublicKey("test_public_zzF3ywvX9DE-dSDNhUqKoaTI4wc");
		 * setup.setToken("ca03737cf94wcf644f36"); setup.setMode("test");
		 * 
		 * //my mpower payment store.setName("Mobilefliks Payment");
		 * store.setTagline("Mobilefliks Paying Process");
		 * store.setPhoneNumber("024000001");
		 * store.setPostalAddress("P. O. Box 17, Dabala");
		 * store.setWebsiteUrl("http://www.mobileflicks.com/");
		 */

		// the tab host section
		TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup();

		// adding the tab
		TabSpec addKickTab = tabHost.newTabSpec("tag1");
		addKickTab.setContent(R.id.addKicktab);
		addKickTab.setIndicator("TRENDING");
		tabHost.addTab(addKickTab);

		// adding the tab
		addKickTab = tabHost.newTabSpec("tag2");
		addKickTab.setContent(R.id.viewKicktab);
		addKickTab.setIndicator("UP COMING");
		tabHost.addTab(addKickTab);

		// adding the tab
		addKickTab = tabHost.newTabSpec("tag2");
		addKickTab.setContent(R.id.viewKicktab);
		addKickTab.setIndicator("MY MOVIES");
		tabHost.addTab(addKickTab);

		// initializing GridView for the My trending movies
		
		
		//trendingMovies.setAdapter(trending);

		// register for contextual menu
		

		// trending contextual menu
		trendingMovies.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Item det =dataItem.get(position);
				Toast.makeText(Home.this, det.getName(),Toast.LENGTH_SHORT).show();
			}
		});
		trendingMovies.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v,
					int position, long id) {
				// TODO Auto-generated method stub
				Item det = dataItem.get(position);
				Toast.makeText(Home.this, det.getName(), Toast.LENGTH_SHORT)
						.show();
				return false;
			}
		});
		// initializing GridView for the My Movies tab

		/*
		 * if (!dataItem.isEmpty()) { mgrid = new ImageLoadAdapter(this, linf,
		 * dataItem, fetchImage); moviesGrid.setAdapter(mgrid); } else {
		 * displayResponse("the size of the array" +
		 * String.valueOf(dataItem.size())); // mgrid=new
		 * ImageAdapter(this,dataItem); }
		 */

		// register for contextual menu
		registerForContextMenu(moviesGrid);

		moviesGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Item det = dataItem.get(position);
				Toast.makeText(Home.this, det.getName(), Toast.LENGTH_SHORT)
						.show();
			}
		});

//		moviesGrid.setOnItemLongClickListener(new OnItemLongClickListener() {
//			@Override
//			public boolean onItemLongClick(AdapterView<?> parent, View v,
//					int position, long id) {
//				// TODO Auto-generated method stub
//				Item det = dataItem.get(position);
//				Toast.makeText(Home.this, det.getName(), Toast.LENGTH_SHORT)
//						.show();
//				return false;
//			}
//		});

		// Initialize GridView for the Up Coming Tab
		GridView upComimgMoviesGrid = (GridView) findViewById(R.id.gridViewUpComingMovies);
		final ImageAdapter upcoming = new ImageAdapter(this, dataItem);
		upComimgMoviesGrid.setAdapter(upcoming);

		upComimgMoviesGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Toast.makeText(Home.this, upcoming.items.get(position).name,
						Toast.LENGTH_SHORT).show();
			}
		});

		viewFlipper = ((ViewFlipper) this.findViewById(R.id.view_flipper));
		viewFlipper.startFlipping();

		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_in));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_out));

		final Object[] data = (Object[]) getLastNonConfigurationInstance();
		if(data!=null){
			this.dataItem=(ArrayList<Item>) data[0];
			this.fetchImage = (MgsImageLoader) data[1];
			moviesGrid.setAdapter(new ImageLoadAdapter(this, this.linf, dataItem, fetchImage));
		}
	}

	// contextual menu onCreate
	public final static String EXTRA_MESSAGE = "url";

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.selectmoviecontextmenu, menu);
	}

	// when a contextual menu item is clicked
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.itemPlay:
			
			Toast.makeText(Home.this, "Coming Soon", Toast.LENGTH_SHORT).show();
//			Item data = dataItem.get(info.position);
//			Toast.makeText(Home.this, "Play Movie: "+data.getName(), Toast.LENGTH_SHORT).show();
//			if(data.getPrice()=="free"){
//			ParseQuery<ParseObject> query = ParseQuery.getQuery("Movies");
//
//			query.getInBackground(data.getUid(),
//					new GetCallback<ParseObject>() {
//						@Override
//						public void done(ParseObject object, ParseException e) {
//							if (e == null) {
//								// object will be your game score
//
//								ParseFile video = object.getParseFile("series");
//								url = video.getUrl();
//
//							} else {
//								// something went wrong
//								Toast.makeText(getApplicationContext(),
//										"Something went wrong",
//										Toast.LENGTH_LONG).show();
//							}
//						}
//					});
//
//			Intent intent = new Intent(this, PlayFlik.class);
//			intent.putExtra(EXTRA_MESSAGE, url);
//			startActivity(intent);
//			}else{
//				createPlayDialog("This is Video is not free,\n Do you want to pay and watch?",data);
//			}
			return true;
		case R.id.itemDownload:

//			Item data1 = dataItem.get(info.position);
//			ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Movies");
//			// final String vidName;
//			query1.getInBackground(data1.uid, new GetCallback<ParseObject>() {
//				@Override
//				public void done(ParseObject object, ParseException e) {
//					if (e == null) {
//						// object will be your game score
//
//						ParseFile video = object.getParseFile("series");
//						url = video.getUrl();
//						videaname = video.getName();
//
//					} else {
//						// something went wrong
//						Toast.makeText(getApplicationContext(),
//								"Something went wrong", Toast.LENGTH_LONG)
//								.show();
//					}
//				}
//			});
//			String credEmail = mprefs.getString("email", "Default");
//			String curr = mprefs.getString("currency", "Default");
//			String vName = mprefs.getString(videaname, "Default");
//			// displayResponse(curr);
//			if (credEmail != "Default" && curr != "Default") {
//				if (android.os.Environment.getExternalStorageState().equals(
//						android.os.Environment.MEDIA_MOUNTED)) {
//					if (vName == "Default") {
//						purchaseItem(videaname,
//								mprefs.getString("currency", "Default"));
//					} else {
//						displayResponse("You have alredy downloaded this movie. Check "
//								+ Environment.getExternalStorageDirectory()
//								+ "/Movies folder");
//					}
//
//				} else {
//					displayResponse("No sdk card detected, please mount one and try again");
//				}
//			} else {
//				Intent regIntent = new Intent(this, RegisterUser.class);
//				startActivityForResult(regIntent, 102);
//			}
			Toast.makeText(Home.this, "Coming Soon", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.itemAddToMyMovies:
			Toast.makeText(Home.this, "Coming Soon", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.itemShare:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					"You can download Mobilefliks at playstore now");
			sendIntent.setType("text/plain");
			startActivity(Intent.createChooser(sendIntent, "Share via:"));
			return true;
		default:
			return super.onContextItemSelected(item);
		}
		// return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflator = getMenuInflater();
		inflator.inflate(R.menu.splash, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.movies:
			break;
		// case R.id.login:
		// startActivity(new Intent(Home.this, WalkThrough.class));
		// break;
		case R.id.series:
			break;
		/*
		 * case R.id.fresh: Intent intent = getIntent();
		 * overridePendingTransition(0, 0);
		 * intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); finish();
		 * overridePendingTransition(0, 0); startActivity(intent); break;
		 */
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	public void purchaseItem(String VideoName, String currency) {
		PayPalPayment payment = new PayPalPayment(new BigDecimal("1.75"),
				"USD", videaname);

		Intent intent = new Intent(this, PaymentActivity.class);

		intent.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_SANDBOX);

		intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AQ5CVxAW4Les7UO1jgoy902NMOclsEZOBvNlRFSnaKcKIUwnbW8mbuQ7Qp32");

		Log.d("Email", mprefs.getString("email", "Default"));
		intent.putExtra(PaymentActivity.EXTRA_PAYER_ID,
				mprefs.getString("email", "Default"));

		intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL,
				"juliet@eagleproductionslimited.com");
		intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

		startActivityForResult(intent, 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			PaymentConfirmation confirm = data
					.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
			if (confirm != null) {
				try {

					Log.i("paymentExample", confirm.toJSONObject().toString(4));
					createDialog("You are about to Download " + videaname
							+ "\nPress OK to Continue");
					displayResponse("Downloading in the background, will notify you when its done");
					currentVersion = android.os.Build.VERSION.SDK_INT;
					if (currentVersion < android.os.Build.VERSION_CODES.GINGERBREAD) {
//						new DownloadTask(this,
//								Environment.getExternalStorageDirectory()
//										+ "/Movies", videaname).execute(url);
					} else {
						new DownloadCls(this,
								Environment.getExternalStorageDirectory()
										+ "/Movies", videaname).execute(url);
					}

					// TODO: send 'confirm' to your server for verification.
					// see
					// https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
					// for more details.

				} catch (JSONException e) {
					Log.e("paymentExample",
							"an extremely unlikely failure occurred: ", e);

				}
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			Log.i("paymentExample", "The user canceled.");

		} else if (resultCode == PaymentActivity.RESULT_PAYMENT_INVALID) {
			Log.i("paymentExample",
					"An invalid payment was submitted. Please see the docs.");

		} else if (resultCode == 102) {
			String message = data.getStringExtra("Result");
			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
					.show();
			String vName = mprefs.getString(videaname, "Default");
			if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED)) {
				if (vName == "Default") {
					purchaseItem(videaname,
							mprefs.getString("currency", "Default"));
				} else {
					displayResponse("You have alredy downloaded this movie. Check the"
							+ Environment.getExternalStorageDirectory()
							+ "/Movies folder");
				}
			} else {
				displayResponse("No sdk card detected, please mount one and try again");
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		stopService(new Intent(this, PayPalService.class));
		super.onDestroy();
	}

	public void displayResponse(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
				.show();
	}

	public void createDialog(String message) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
					}
				}).create();
		alert.show();

	}
	public void createPlayDialog(String message,final Item data) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						getpurch(data);
					}
				}).setNegativeButton("No", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
					}
				}).create();
		alert.show();

	}

	public void getpurch(Item data){
		String credEmail = mprefs.getString("email", "Default");
		//String curr = mprefs.getString("currency", "Default");
		String vName = mprefs.getString(videaname, "Default");
		if (credEmail != "Default" ) {
		purchaseItem(videaname,"USD");
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Movies");

		query.getInBackground(data.getUid(),
				new GetCallback<ParseObject>() {
					@Override
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							// object will be your game score

							ParseFile video = object.getParseFile("series");
							url = video.getUrl();

						} else {
							// something went wrong
							Toast.makeText(getApplicationContext(),
									"Something went wrong",
									Toast.LENGTH_LONG).show();
						}
					}
				});

		Intent intent = new Intent(this, PlayFlik.class);
		intent.putExtra(EXTRA_MESSAGE, url);
		startActivity(intent);
		
		}else {
			Intent regIntent = new Intent(this, RegisterUser.class);
			startActivityForResult(regIntent, 102);
		}
	}
	
	
	public void saveMovieCredentials(String name) {
		SharedPreferences.Editor meditor = mprefs.edit();
		meditor.putString(name, name);
		meditor.commit();
	}

	@Override
	@Deprecated
	public Object onRetainNonConfigurationInstance() {
		// TODO Auto-generated method stub
		Object[] mystuff = new Object[2];
		mystuff[0] = this.dataItem;
		mystuff[1] = this.fetchImage;
		return mystuff;
	}

	public static class MyViewHolder {
		public ImageView image;
		public TextView name;
		public Item data;

	}

	public void setImageIngrid(ArrayList<Item> param) {
		tvLoading.setVisibility(View.GONE);
		this.dataItem = param;
		//trending=new ImageLoadAdapter(this, linf, dataItem,this.fetchImage);
		 trendingMovies.setAdapter(new ImageLoadAdapter(this, linf, dataItem,this.fetchImage));
		// moviesGrid.setAdapter(new ImageLoadAdapter(this, linf, dataItem,this.fetchImage));
		 registerForContextMenu(trendingMovies);
		 //registerForContextMenu(moviesGrid);
	}

	public void loadParse() {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Movies");
		query.whereEqualTo("Trending", true);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> movieList, ParseException e) {
				if (e == null) {
					for (ParseObject movies : movieList) {

						/*displayResponse(movies.getString("title")
								+ " \nDescription: " + movies.getString("gist"));*/
						ParseFile video = movies.getParseFile("series");
						url = video.getUrl();
						dataItem.add(new Item(movies.getString("title"), movies
								.getObjectId(), url, movies.getString("Price"),
								movies.getParseFile("images").getUrl(),movies.getString("gist")));
						Log.d(movies.getString("title"),
								movies.getString("gist"));
					}

				} else {
					System.out.println(e.getMessage());
				}
				/*displayResponse("The size of the arrayList "
						+ String.valueOf(dataItem.size()));*/
				if (dataItem.size() > 0) {
					tvLoading.setVisibility(View.GONE);
					Retry.setVisibility(View.GONE);
					setImageIngrid(dataItem);
				} else {
					displayResponse(" No Connection ");
					tvLoading.setVisibility(View.GONE);
					Retry.setVisibility(View.VISIBLE);
				}
			}
		});
		Log.i("ArrayList Size",
				"the size of the array is" + String.valueOf(dataItem.size()));

	}

	public static class MyVHolder {
		public ImageView icon;
		public TextView name;
		public Item dataItem;
	}

	public void setimagebyte(byte[] img) {
		this.img1 = img;
	}

	public byte[] getimagebyte() {
		return this.img1;
	}

}
