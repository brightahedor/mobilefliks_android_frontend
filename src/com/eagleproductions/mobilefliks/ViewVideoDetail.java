package com.eagleproductions.mobilefliks;

import java.math.BigDecimal;

import org.json.JSONException;

import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.Adapter.ImageLoader;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewVideoDetail extends Activity{

	Button play,download,addtomymoview,share;
	String Price,ObjectID,imgUrl,videourl;
	String name;
	TextView detail,tvprice;
	public final static String EXTRA_MESSAGE = "url";
	SharedPreferences mprefs;
	AlertDialog alert;
	 public ImageLoader imgLoader;
	 ImageView img;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Intent intent1 = new Intent(this, PayPalService.class);

		intent1.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_LIVE);
		intent1.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AY7N4BAIo6Qj4aiNJDSvwbEi2UvCuwBs4M0g_McCdjAkUWdBbTlxUb8vzcGc");

		startService(intent1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.videolsyout);
		mprefs = getSharedPreferences("Credentials", MODE_PRIVATE);
		play=(Button)findViewById(R.id.btnPlay);
		download=(Button)findViewById(R.id.btnDownload);
		addtomymoview=(Button)findViewById(R.id.addTomyMovies);
		share=(Button)findViewById(R.id.btnShare);
		detail=(TextView)findViewById(R.id.txtDetail);
		tvprice=(TextView)findViewById(R.id.txtPrice);
		img=(ImageView)findViewById(R.id.ivImageDisp);
		img.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imgLoader=new ImageLoader(this);
		getData();
		play.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Bundle group=new Bundle();
				group.putString("VideoName", name);
				group.putString("VideoUrl", videourl);
				group.putString("Price", tvprice.getText().toString());
						
				Intent intent = new Intent(ViewVideoDetail.this, PlayFlik.class);
				intent.putExtras(group);
				startActivity(intent);
			}
		});
		
		download.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			   if(ViewVideoDetail.this.Price.equals("free")){
				   if (android.os.Environment.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED)) {
					   new DownloadCls(ViewVideoDetail.this, Environment.getExternalStorageDirectory()+ "/Movies", name).execute(videourl);
					   //new DownloadTask(ViewVideoDetail.this,Environment.getExternalStorageDirectory()+ "/Movies", name).execute(videourl);
					   //String vName = mprefs.getString(name, "Default");
					  /* int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					   if (currentapiVersion > android.os.Build.VERSION_CODES.FROYO){
					       // Do something for froyo and above versions
						   
					   } else{
					       // do something for phones running an SDK before froyo
						   createDialog("Downloading in the background,you will be notified  when its done");
						   
					   }*/
							
						

					} else {
						displayResponse("No sdk card detected, please mount one and try again");
					}
				   
			   }else{
			createPlayDialog("Downloading this Video will cost you $"+Price+"\n Would you Like to continue?");
			   }
			}

			
		});
		
		addtomymoview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				displayResponse("Feature will be added in the next version");
			}
		});
		
		share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent sendIntent = new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_TEXT,
						"You can download Mobilefliks at playstore now");
				sendIntent.setType("text/plain");
				startActivity(Intent.createChooser(sendIntent, "Share via:"));
			}
		});
	}
	private void download() {
		// TODO Auto-generated method stub
		String credEmail = mprefs.getString("email", "Default");
		//String curr = mprefs.getString("currency", "Default");
		String vName = mprefs.getString(name, "Default");
		// displayResponse(curr);
		if (credEmail != "Default") {
			if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED)) {
				
					purchaseItem(name,
							mprefs.getString("currency", "Default"));
				

			} else {
				displayResponse("No sdk card detected, please mount one and try again");
			}
		} else {
			Intent regIntent = new Intent(this, RegisterUser.class);
			startActivityForResult(regIntent, 102);
		}
	}
	private void getData() {
		// TODO Auto-generated method stub
	   
		Bundle getGroup=getIntent().getExtras();
		name=getGroup.getString("Name");
		String desc=getGroup.getString("Description");
		ObjectID=getGroup.getString("ObjectID");
			Price=(getGroup.getString("Price"));
		imgUrl=(getGroup.getString("ImageUrl"));
		videourl=(getGroup.getString("VideoUrl"));
		imgLoader.DisplayImage(imgUrl, img);
		detail.setText((name+"\n"+desc));
		tvprice.setText(Price);
		
		
	}

	public void purchaseItem(String VideoName, String currency) {
		PayPalPayment payment = new PayPalPayment(new BigDecimal(Price),
				currency, VideoName);

		Intent intent = new Intent(this, PaymentActivity.class);

		intent.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_SANDBOX);

		intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AQ5CVxAW4Les7UO1jgoy902NMOclsEZOBvNlRFSnaKcKIUwnbW8mbuQ7Qp32");

		Log.d("Email", mprefs.getString("email", "Default"));
		intent.putExtra(PaymentActivity.EXTRA_PAYER_ID,
				mprefs.getString("email", "Default"));

		intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL,
				"juliet@eagleproductionslimited.com");
		intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

		startActivityForResult(intent, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		 if (resultCode == Activity.RESULT_OK) {
	            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
	            if (confirm != null) {
	                try {
	                    Log.i("paymentExample", confirm.toJSONObject().toString(4));
	                    new DownloadCls(ViewVideoDetail.this, Environment.getExternalStorageDirectory()+ "/Movies", name).execute(videourl);
	                    //displayResponse("Downloading in the background,you will be notified  when its done");
	                    /*int currentapiVersion = android.os.Build.VERSION.SDK_INT;
						   if (currentapiVersion > android.os.Build.VERSION_CODES.FROYO){
						       // Do something for froyo and above versions
							   
						   } else{
						       // do something for phones running an SDK before froyo
							   createDialog("Downloading in the background,you will be notified  when its done");
							   
						   }*/
						

	                } catch (JSONException e) {
	                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
	                }
	            }
	        }
	        else if (resultCode == Activity.RESULT_CANCELED) {
	            Log.i("paymentExample", "The user canceled.");
	        }
	        else if (resultCode == PaymentActivity.RESULT_PAYMENT_INVALID) {
	            Log.i("paymentExample", "An invalid payment was submitted. Please see the docs.");
	        }else if (resultCode == 102) {
				String message = data.getStringExtra("Result");
				Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
						.show();
				//String vName = mprefs.getString(name, "Default");
				if (android.os.Environment.getExternalStorageState().equals(
						android.os.Environment.MEDIA_MOUNTED)) {
					
						purchaseItem(name,
								"USD");
					
				} else {
					displayResponse("No sdk card detected, please mount one and try again");
				}
			}
	}
	
	public void createDialog(String message) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
					}
				}).create();
		alert.show();

	}
	
	public void displayResponse(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
				.show();
	}
	
	public void saveMovieCredentials(String name) {
		SharedPreferences.Editor meditor = mprefs.edit();
		meditor.putString(name, name);
		meditor.commit();
	}
	
	 @Override
	    public void onDestroy() {
	        stopService(new Intent(this, PayPalService.class));
	        super.onDestroy();
	    }
	 
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		super.onBackPressed();
	}
	
	 public void createPlayDialog(String message) {
			alert = new AlertDialog.Builder(this).setTitle("Information!")
					.setMessage(message)
					.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							purchaseItem(name, "USD");
						}
					}).setNegativeButton("No", new AlertDialog.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							alert.cancel();
						}
					}).create();
			alert.show();

		}
}
