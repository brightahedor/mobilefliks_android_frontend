package com.eagleproductions.mobilefliks;

import java.util.ArrayList;
import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.VideoFiles.MyViewHolder;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore.Video.Thumbnails;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class VideoListAdapter extends BaseAdapter{
    private VideoFiles act;
    private LayoutInflater infl;
    private ArrayList<DataCredentials> data;
    
	public VideoListAdapter(VideoFiles act, LayoutInflater infl,
			ArrayList<DataCredentials> data) {
		super();
		this.act = act;
		this.infl = infl;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int pos, View convertview, ViewGroup parent) {
		// TODO Auto-generated method stub
		MyViewHolder holder;
		if(convertview ==null){
			convertview=infl.inflate(R.layout.videofiles, parent,false);
			holder=new MyViewHolder();
			holder.img=(ImageView) convertview.findViewById(R.id.Thumbnail);
			holder.name=(TextView)convertview.findViewById(R.id.FilePath);
			convertview.setTag(holder);
		}else{
			holder=(MyViewHolder) convertview.getTag();
		}
		DataCredentials dt=data.get(pos);
		holder.blueprint=dt;
		holder.name.setText(dt.getVideoname());
		Bitmap bmThumbnail;
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(dt.getVideopath(), Thumbnails.MICRO_KIND);
        holder.img.setImageBitmap(bmThumbnail);
		return convertview;
	}

}
