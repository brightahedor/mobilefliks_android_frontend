package com.eagleproductions.mobilefliks;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class DownloadTask extends AsyncTask<String, String, String> {
    ViewVideoDetail act;
    String rootDir;
    String name;
    int count;
    private ProgressDialog pDialog;
    
	public DownloadTask(ViewVideoDetail act, String rootDir, String name) {
		super();
		this.act = act;
		this.rootDir = rootDir;
		this.name = name;
		
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		 pDialog = new ProgressDialog(act);
         pDialog.setMessage("Downloading file. Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setMax(100);
         pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
         pDialog.setCancelable(true);
         pDialog.show();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		 
		 if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED)) {
				// sd card mounted
				Log.i("Home", "SDCard Mounted");
				File direct = new File(Environment.getExternalStorageDirectory()
						+ "/Movies");

				if (!direct.exists()) {
					if (direct.mkdir()) {
						// directory is created;
						fileDownload(params[0]);
                          
						Log.i("Home", "directory exits");
					} else {
						direct.mkdirs();
						Log.i("Home", "directory created");
						fileDownload(params[0]);
					}

				} else {
					direct.mkdirs();
					Log.i("Home", "directory crreated");
						fileDownload(params[0]);
					}
					
				}else {
					
					return "No sdcard Detected, Please make sure you have one inserted and try again";

			}
	       
	 
	        return null;
	 
	}

	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		pDialog.setProgress(Integer.parseInt(values[0]));
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		
		pDialog.dismiss();
		if (result==null){
			act.saveMovieCredentials(name);
		act.createDialog(name+" has finished downloading. Go to"+rootDir+"/"+name+" to watch");
		//act.displayResponse(name+" has finished downloading. Go to"+rootDir+"/"+name+" to watch");
		
		}else{
			act.createDialog(result);
		}
	}
	
	public void fileDownload(String ar01){
		 try {
	            URL url = new URL(ar01);
	            URLConnection conection = url.openConnection();
	            conection.connect();
	            // getting file length
	            int lenghtOfFile = conection.getContentLength();
	 
	            // input stream to read file - with 8k buffer
	            InputStream input = new BufferedInputStream(url.openStream(), 8192);
	 
	            // Output stream to write file
	            OutputStream output = new FileOutputStream(rootDir+"/"+name);
	 
	            byte data[] = new byte[1024];
	 
	            long total = 0;
	 
	            while ((count = input.read(data)) != -1) {
	                total += count;
	                // publishing the progress....
	                // After this onProgressUpdate will be called
	                publishProgress(""+(int)((total*100)/lenghtOfFile));
	 
	                // writing data to file
	                output.write(data, 0, count);
	            }
	 
	            // flushing output
	            output.flush();
	 
	            // closing streams
	            output.close();
	            input.close();
	 
	        } catch (Exception e) {
	            Log.e("Error: ", e.getMessage());
	        }
		
	}

}
