package com.eagleproductions.mobilefliks.Adapter;

public class Item {
	public String name;
	public String uid;
   public String price;
	String drawableId;
	 int drId;
	 String imgdata;
	 public String Des;

	public Item(String name, String uid, String drawableId,String price,String imgdata,String Description) {
		this.name = name;
		this.drawableId = drawableId;
		this.uid = uid;
		this.price=price;
		this.imgdata=imgdata;
		Des=Description;
	}

	public Item(String name2, String uid2, int midNight) {
		// TODO Auto-generated constructor stub
		this.name = name2;
		this.uid=uid2;
		this.drId=midNight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDrawableId() {
		return drawableId;
	}

	public void setDrawableId(String drawableId) {
		this.drawableId = drawableId;
	}

	public int getDrId() {
		return drId;
	}

	public void setDrId(int drId) {
		this.drId = drId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getImgdata() {
		return imgdata;
	}

	public void setImgdata(String imgdata) {
		this.imgdata = imgdata;
	}

	public String getDes() {
		return Des;
	}

	public void setDes(String des) {
		Des = des;
	}
	

}
