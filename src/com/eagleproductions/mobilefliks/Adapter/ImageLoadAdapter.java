package com.eagleproductions.mobilefliks.Adapter;

import java.util.ArrayList;

import com.eagleproductions.mobilefliks.DataDetails;
import com.eagleproductions.mobilefliks.Home;
import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.ViewVideoDetail;
import com.eagleproductions.mobilefliks.Home.MyVHolder;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;



import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageLoadAdapter extends BaseAdapter implements OnClickListener{
     Home act;
     LayoutInflater inf;
     public ArrayList<Item> data;
    // private MgsImageLoader imgfetch;
     public ImageLoader imgLoader;
     
	public ImageLoadAdapter(Home act, LayoutInflater inf,
			ArrayList<Item> data,MgsImageLoader imgfetch) {
		super();
		this.act = act;
		this.inf = inf;
		this.data = data;
		this.imgLoader=new ImageLoader(act.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parentGroup) {
		// TODO Auto-generated method stub
		MyVHolder holder = null;
		if(convertView==null){
			holder=new MyVHolder();
			convertView=inf.inflate(R.layout.griditemlayout, parentGroup, false);
			holder.icon=(ImageView) convertView.findViewById(R.id.picture);
			holder.name=(TextView) convertView.findViewById(R.id.text);
			holder.icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
			holder.icon.setPadding(5,5,5,5);
			convertView.setTag(holder);
		}else{
			holder=(MyVHolder)convertView.getTag();
		}
	Item it=data.get(position);
	holder.dataItem=it;
	convertView.setOnClickListener(this);
	 holder.name.setText(it.getName());
		if(it.getImgdata()!=null){
			imgLoader.DisplayImage(it.getImgdata(), holder.icon);
			
			
		}
		return convertView;
	}

	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	MyVHolder holder=(MyVHolder)v.getTag();
	if(v instanceof View){
		Bundle group=new Bundle();
		group.putString("Name", holder.dataItem.getName());
		group.putString("Description", holder.dataItem.getDes());
		group.putString("Price", holder.dataItem.getPrice());
		group.putString("ObjectID", holder.dataItem.getUid());
		group.putString("ImageUrl", holder.dataItem.getImgdata());
		group.putString("VideoUrl", holder.dataItem.getDrawableId());
		try{
			Intent ourIntent = new Intent(this.act,ViewVideoDetail.class);
			ourIntent.putExtras(group);
			this.act.startActivity(ourIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	}

}
