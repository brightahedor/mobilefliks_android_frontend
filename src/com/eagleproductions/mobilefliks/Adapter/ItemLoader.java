package com.eagleproductions.mobilefliks.Adapter;

import java.util.ArrayList;
import java.util.List;

import com.eagleproductions.mobilefliks.Home;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;



import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class ItemLoader extends AsyncTask<Void, Integer, String> {

	
   private ProgressDialog progDialog;
	Home act;
	ArrayList<Item> items =new ArrayList<Item>();
	String url;
    
	public ItemLoader( Home act) {
		super();
		
		this.act = act;
		
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		progDialog=new ProgressDialog(this.act);
		progDialog.setMessage("Loading..");
		progDialog.setIndeterminate(false);
		progDialog.setCancelable(false);
		progDialog.show();
	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Movies");
		System.out.println("parse query");
		query.whereEqualTo("Trending", true);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> movieList, ParseException e) {
				if (e == null) {
					for (ParseObject movies : movieList) {

						/*displayResponse(movies.getString("title")
								+ " \nDescription: " + movies.getString("gist"));*/
						ParseFile video = movies.getParseFile("series");
						url = video.getUrl();
//						items.add(new Item(movies.getString("title"), movies
//								.getObjectId(), url, movies.getString("Price"),
//								movies.getParseFile("images").getUrl()));
						Log.d(movies.getString("title"),
								movies.getString("gist"));
					}

				} else {
					System.out.println(e.getMessage());
				}
				/*displayResponse("The size of the arrayList "
						+ String.valueOf(dataItem.size()));*/
				
				
			}
		});

		return "Done";
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		progDialog.dismiss();
		act.setImageIngrid(items);
	}

}
