package com.eagleproductions.mobilefliks;

import java.io.File;

import com.paypal.android.sdk.i;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class DownloadCls extends AsyncTask<String, Integer, Long> {
	Context ctx;
	String rootDir;
	String fileTitle;
    DatabaseCls locdb;
    public DownloadCls(Context context,String path, String name){
    	ctx=context;
    	rootDir=path;
    	fileTitle=name;
    	locdb=new DatabaseCls(context);
    }
	@SuppressLint("NewApi")

	@Override
	protected Long doInBackground(String... params) {
		// TODO Auto-generated method stub
		DownloadManager dm = (DownloadManager) ctx
				.getSystemService(Context.DOWNLOAD_SERVICE);
		//boolean isDownloading = false;
		long id = 0;
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {
			File direct = new File(rootDir);
			if (!direct.exists()) {
				if (direct.mkdir()) {
					// directory is created;
					String serviceRequest = Context.DOWNLOAD_SERVICE;
					
					
					Uri url = Uri.parse(params[0]);
					Uri destination = Uri.fromFile(new File(rootDir));
					DownloadManager.Request request = new Request(url);
					String mimeType = "VIDEO/MP4";
					request.setMimeType(mimeType);
					//request.setAllowedNetworkTypes(Request.NETWORK_WIFI);
					request.setTitle(fileTitle);
					request.setDescription(fileTitle);
					request.setDestinationUri(destination);
					request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
					id = dm.enqueue(request);
					
				} else {
					direct.mkdirs();
					Uri url = Uri.parse(params[0]);
					Uri destination = Uri.fromFile(new File(rootDir));
					DownloadManager.Request request = new Request(url);
					String mimeType = "VIDEO/MP4";
					request.setMimeType(mimeType);
					//request.setAllowedNetworkTypes(Request.NETWORK_WIFI);
					request.setTitle(fileTitle);
					request.setDescription(fileTitle);
					request.setDestinationUri(destination);
					request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
					id = dm.enqueue(request);
				}
		
		
		
	
		}else{
			Uri url = Uri.parse(params[0]);
			Uri destination = Uri.fromFile(new File(rootDir));
			DownloadManager.Request request = new Request(url);
			String mimeType = "VIDEO/MP4";
			request.setMimeType(mimeType);
			//request.setAllowedNetworkTypes(Request.NETWORK_WIFI);
			request.setTitle(fileTitle);
			request.setDescription(fileTitle);
			request.setDestinationUri(destination);
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
			id = dm.enqueue(request);

		}
			
	}
		return id;
	}

	@Override
	protected void onPostExecute(Long result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(result>0){
			Toast.makeText(ctx,fileTitle+ " is Downloading.", Toast.LENGTH_LONG).show();
		}
	}

	class DownloadReceiver extends BroadcastReceiver {
        
		
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			long receivedID = arg1.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, -1L);
			DownloadManager mgr = (DownloadManager) arg0
					.getSystemService(Context.DOWNLOAD_SERVICE);

			DownloadManager.Query query = new DownloadManager.Query();
			query.setFilterById(receivedID);
			Cursor cur = mgr.query(query);
			int index = cur.getColumnIndex(DownloadManager.COLUMN_STATUS);
			if (cur.moveToFirst()) {
				if (cur.getInt(index) == DownloadManager.STATUS_SUCCESSFUL) {
                        locdb.open();
                        locdb.createEntry(rootDir+"/"+fileTitle,fileTitle);
                        locdb.close();
				}
			}
			cur.close();
		}

	}
	}

		
	

